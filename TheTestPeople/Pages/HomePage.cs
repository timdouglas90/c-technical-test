﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace TheTestPeople.Pages
{
    public class HomePage
    {
        public static IWebDriver firefoxInstance = new FirefoxDriver();

        public void NavigateToWikipedia()
        {
            firefoxInstance.Navigate().GoToUrl("https://www.wikipedia.org/");
        }

        public void PerformSearch(string searchTerm, string language)
        {
            IWebElement searchInputBox = firefoxInstance.FindElement(By.Id("searchInput"));
            searchInputBox.SendKeys(searchTerm);

            IWebElement languageDropDownListBox = firefoxInstance.FindElement(By.Id("searchLanguage"));
            SelectElement clickThis = new SelectElement(languageDropDownListBox);
            clickThis.SelectByText(language);

            searchInputBox.Submit();
        }
    }
}
