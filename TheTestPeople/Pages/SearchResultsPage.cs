﻿using System;
using OpenQA.Selenium;

namespace TheTestPeople.Pages
{
    public class SearchResultsPage
    {
        public string Title(string searchText)
        {
            var searchResultsTitle = HomePage.firefoxInstance.Title;

            if (searchResultsTitle.Contains(searchText))
            {
                return searchResultsTitle;
            }
            return "FAIL";
        }

        public void ChangePageLanguage()
        {
            HomePage.firefoxInstance.FindElement(By.CssSelector("a[href*='//de.wikipedia.org/wiki/K%C3%A4se']")).Click();
        }

        public bool ContainsOldLanguage()
        {
            try
            {
                HomePage.firefoxInstance.FindElement(By.CssSelector("a[href*='//en.wikipedia.org/wiki/Cheese']"));
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}
