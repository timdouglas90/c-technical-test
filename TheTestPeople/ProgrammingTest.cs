﻿using System;

namespace TheTestPeople
{
    class ProgrammingTest
    {
        static void Main(string[] args)
        {
        }

        public int UnorderedArray(int[] a)
        {
            int result = 0;
            int current = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (i > 0 && a[i] <= a[i - 1])
                    current = 0;
                current++;
                result = Math.Max(result, current);
            }
            return result;
        }
    }
}
