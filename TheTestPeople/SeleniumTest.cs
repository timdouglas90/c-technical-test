﻿using Xunit;

namespace TheTestPeople.Pages
{
    public class SeleniumTest
    {
        HomePage homePage = new HomePage();
        SearchResultsPage searchResultsPage = new SearchResultsPage();

        [Fact]
        public void WikipediaSearch()
        {
            const string searchText = "Cheese";
            const string language = "English";
            const string searchResultsTitle = "Cheese - Wikipedia, the free encyclopedia";

            homePage.NavigateToWikipedia();
            homePage.PerformSearch(searchText, language);
            Assert.True(searchResultsPage.Title(searchText) == searchResultsTitle);

            searchResultsPage.ChangePageLanguage();
            Assert.True(searchResultsPage.ContainsOldLanguage());

            HomePage.firefoxInstance.Close();
        }
    }
}
