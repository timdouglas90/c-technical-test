﻿using Xunit;

namespace TheTestPeople.UnitTests
{
    public class GivenAnUnorderedArrayTest
    {
        [Fact]
        public void Example1()
        {
            ProgrammingTest program = new ProgrammingTest();

            Assert.Equal(4, program.UnorderedArray(new int[]{ 1, 4, 1, 4, 2, 1, 3, 5, 6, 2, 3, 7}));
        }

        [Fact]
        public void Example2()
        {
            ProgrammingTest program = new ProgrammingTest();

            Assert.Equal(3, program.UnorderedArray(new int[] { 3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5 }));
        }

        [Fact]
        public void Example3()
        {
            ProgrammingTest program = new ProgrammingTest();

            Assert.Equal(2, program.UnorderedArray(new int[] { 2, 7, 1, 8, 2, 8, 1 }));
        }

        [Fact]
        public void Example4()
        {
            ProgrammingTest program = new ProgrammingTest();

            Assert.Equal(1, program.UnorderedArray(new int[] { 2, 1, 1, 1 }));
        }
    }
}
