﻿using Xunit;

namespace TheTestPeople.UnitTests
{
    public class WebServiceTests
    {
        [Theory]
        [InlineData("West Midlands")]
        [InlineData("Greater Manchester")]
        public void SOAPServiceContainsALocation(string county)
        {
            var sut = new WebServiceTest();

            Assert.True(sut.GetUKLocationByCounty(county));

        }
    }
}
