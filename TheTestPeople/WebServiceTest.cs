﻿namespace TheTestPeople
{
    class WebServiceTest
    {
        public bool GetUKLocationByCounty(string county)
        {
            UKLocationService.UKLocationSoapClient client = new UKLocationService.UKLocationSoapClient();

            var response = client.GetUKLocationByCounty(county);

            if(response.Contains("Copt Heath") || response.Contains("Clough"))
            {
                return true;
            }
            return false;
            //This is super hacky, I'm not entirely comfortable with this incase you can't tell!
        }
    }
}
